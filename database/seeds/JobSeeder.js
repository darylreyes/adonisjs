'use strict'

/*
|--------------------------------------------------------------------------
| JobSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Job = use('App/Models/Job')
class JobSeeder {
  async run () {
    await Job.createMany([{
      title: "TestTitle 1",
      link: 'http://google.com',
      descriptions: "descriptions 1"
  },
  {
      title: "TestTitle 2",
      link: 'http://google.com',
      descriptions: "descriptions 3"
  },
  {
    title: "TestTitle 3",
    link: 'http://google.com',
    descriptions: "descriptions 3"
}

])
  }
}

module.exports = JobSeeder


